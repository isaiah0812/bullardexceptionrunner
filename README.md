1. The logger gives specific information about the runtime of the program with the severity of certain issues and filter the output based on that severity, where as the console displays everything and doesn't have that extra information as to the severity of the information described.
2. It comes from JUnit, saying that a certain line isn't present.
3. It tests to determine if a particular function will throw an exception. It's the Timer Exception.
4. a) It is used in the serialization class as a version control that is made just to deserialize when the program is running.
   b) That way the specific sub-types of an object can be initialized in different ways, depending on the extra variables they have in addition to those inherited from the super-class.
   c) These methods aren't needed.
5. It will load the logger properties from logger.properties when the class loads.
6. The file can contain a markdown and the .md makes it have the ability to be displayed on the overview page.
7. There is actually a NullPointerException coming from the function because timeNow is null when it is called in the finally block, so we must assign it to System.currentTimeMillis() before the try-catch-finally blocks to ensure that it is not null when it gets there.
8. When the tests enter the timeMe function, timeToWait is checked to see if it is less than 0. If so, it goes to the finally block, and prints the statements in the block as info to the logger if timeNow is not null. If timeToWait is 0 or greater, then it will call method with timeToWait, which should be set to System.timeInMillis(). If method throws a InterruptedException, there is a catch block that will handle the exception, but the message will not print to the logger, since it's filter is severe. The catch block will then throw a TimerException with the InterrupttedException being the cause. Regardless of whether the InerruptedException is caught or not, the function will then execute the same process of the finally block as stated before.
9. Listed in repository as �Number09.pdf�
10. Listed in repository as �Number10.pdf�
11. TimerException is a checked exception, while NullPointerException is a runtime exception.
12. git clone https://isaiah0812@bitbucket.org/isaiah0812/bullardexceptionrunner.git
